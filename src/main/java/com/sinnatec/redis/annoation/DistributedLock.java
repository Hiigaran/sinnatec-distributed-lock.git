package com.sinnatec.redis.annoation;

import java.lang.annotation.*;

/**
 * @ClassName DistributedLock
 * @Description 分布式锁
 * @Author fangsong
 * @Date 2021/12/8 10:34
 * @Version 1.0
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DistributedLock {
    String lockName();
    long waitTime() default 5000L;
}
