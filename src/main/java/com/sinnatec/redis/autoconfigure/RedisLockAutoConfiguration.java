package com.sinnatec.redis.autoconfigure;


import com.sinnatec.redis.RedisLockService;
import com.sinnatec.redis.annoation.DistributedLockAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
public class RedisLockAutoConfiguration {

    @Bean
    public RedisLockService redisLockService(StringRedisTemplate stringRedisTemplate){
        return new RedisLockService(stringRedisTemplate);
    }

    @Bean
    public DistributedLockAspect distributedLockAspect(RedisLockService redisLockService){
        return new DistributedLockAspect(redisLockService);
    }

}
