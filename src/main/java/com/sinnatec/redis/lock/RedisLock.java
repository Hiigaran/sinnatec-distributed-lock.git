package com.sinnatec.redis.lock;

/**
 * @ClassName RedisLock
 * @Description redis分布式锁
 * @Author fangsong
 * @Date 2021/12/6 15:22
 * @Version 1.0
 */
public interface RedisLock {

    /**
     * 加锁，直接返回成功或者失败
     * @return
     *              是否加锁成功
     */
    boolean getLock();

    /**
     *
     * 尝试加锁，使用默认超时时长，超时后返回false
     *
     * @return
     *              是否加锁成功
     */
    boolean tryLock();

    /**
     * 尝试加锁，超时后返回false
     *
     * @param timeout
     *                  超时时长，如果超过此时间没有获取锁则返回false，单位毫秒，小于0代表不会超时，一直等待
     *
     * @return      是否加锁成功
     */
    boolean tryLock(long timeout);

    /**
     * 释放锁
     *
     * @return
     *              是否释放成功
     */
    boolean releaseLock();

}
