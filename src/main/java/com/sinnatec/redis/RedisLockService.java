package com.sinnatec.redis;

import org.springframework.data.redis.core.StringRedisTemplate;
import com.sinnatec.redis.lock.RedisLock;
import com.sinnatec.redis.lock.impl.ReentrantLock;
/**
 * @ClassName RedisService
 * @Description redis操作类
 * @Author fangsong
 * @Date 2021/11/17 11:27
 * @Version 1.0
 */
public class RedisLockService {

    private StringRedisTemplate stringRedisTemplate;

    public RedisLockService(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 获取分布式锁操作对象
     *
     * @return
     *                  分布式锁操作对象
     */
    public RedisLock getLock(String lockName){
        return new ReentrantLock(stringRedisTemplate, lockName);
    }
}
